def buildApp() {
  echo "building the application, version ${NEW_VERSION} ..."
}

def testApp() {
  echo 'testing the application ...' 
}

def deployApp() {
  echo "deploying the application ${params.VERSION}..."
}

return this